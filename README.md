CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Support requests
* Maintainers


INTRODUCTION
------------

Engaging Networks provides a service for interaction with the [Engaging Networks Services (ENS) APIs](https://www.engagingnetworks.support/knowledge-base/engaging-networks-services-ens/).
Note the [REST API](https://www.engagingnetworks.support/knowledge-base/engaging-networks-services-ens/)
is the only currently supported API service.


REQUIREMENTS
------------

This module requires the following modules:

* [Key](https://www.drupal.org/project/key) - For storing API access
  credentials.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for details.


CONFIGURATION
-------------

Configure API authentication settings by navigating to Configuration > Engaging
Networks > REST API (`/admin/config/engaging-networks/settings/rest-api`).


USAGE
------------

The Engaging Networks module provides a single service -- the REST API client.

```php
/** @var \Drupal\engaging_networks\RestApi $client */
$restApi = Drupal::service('engaging_networks.rest_api');
$page = $restApi->getClient()->getPage(1234);
```


SUPPORT REQUESTS
----------------

* Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

* Before posting a support request, check the Recent Log entries at
  `/admin/reports/dblog`.

* Once you have done this, you can post a support request at module issue
  queue: [https://www.drupal.org/project/issues/engaging_networks](https://www.drupal.org/project/issues/engaging_networks)

* When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.


MAINTAINERS
-----------

Current maintainers:

* [Christopher C. Wells (wells)](https://www.drupal.org/u/wells)

Development sponsored by:

* [Cascade Public Media](https://www.drupal.org/cascade-public-media)
