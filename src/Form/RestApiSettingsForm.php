<?php

namespace Drupal\engaging_networks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a general settings form for Engaging Networks REST API.
 *
 * @package Drupal\engaging_networks\Form
 */
final class RestApiSettingsForm extends ConfigFormBase {

  /**
   * Gets the name of the config the form modifies.
   */
  private static function getConfigName(): string {
    return 'engaging_networks.rest_api';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::getConfigName()];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'engaging_networks_rest_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory->get(self::getConfigName());
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('API endpoint to use for queries'),
      '#default_value' => $config->get('endpoint'),
      '#disabled' => $this->configIsOverridden('endpoint'),
    ];
    $form['api_key'] = [
      '#title' => 'API key',
      '#type' => 'key_select',
      '#empty_option' => $this->t('- Select -'),
      '#key_filters' => ['type' => 'authentication'],
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('api_key'),
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];
    $form['advanced']['cache_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cache tokens'),
      '#description' => $this->t('Store tokens in state cache for usage across multiple requests.'),
      '#default_value' => $config->get('cache_enable'),
      '#disabled' => $this->configIsOverridden('cache_enable'),
    ];
    $form['advanced']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token cache key'),
      '#description' => $this->t('Cache key (identifier) to use for caching the API token.'),
      '#default_value' => $config->get('cache_keys.token'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('cache_keys.token'),
    ];
    $form['advanced']['expire'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token expire cache key'),
      '#description' => $this->t('Cache key (identifier) to use for caching the API token expire time.'),
      '#default_value' => $config->get('cache_keys.expire'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('cache_keys.expire'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config($this->getConfigName());
    foreach (['endpoint', 'api_key', 'cache_enable'] as $key) {
      if (!$this->configIsOverridden($key)) {
        $config->set($key, $form_state->getValue($key));
      }
    }
    foreach (['token', 'expire'] as $key) {
      if (!$this->configIsOverridden("cache_keys.$key")) {
        $config->set("cache_keys.$key", $form_state->getValue($key));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Check if a config variable is overridden by local settings.
   */
  protected function configIsOverridden(string $setting_name): bool {
    return $this->configFactory->getEditable(self::getConfigName())->get($setting_name)
      != $this->configFactory->get(self::getConfigName())->get($setting_name);
  }

}
