<?php

namespace Drupal\engaging_networks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\State\StateInterface;
use Drupal\key\KeyRepositoryInterface;
use OpenPublicMedia\EngagingNetworksServices\Rest\Client;

/**
 * Engaging Networks Services REST API client.
 *
 * @package Drupal\engaging_networks
 */
final class RestApi {

  /**
   * Engaging Networks config.
   */
  private ImmutableConfig $config;

  /**
   * Engaging Networks REST API client.
   */
  private Client $client;

  /**
   * Constructs the API client wrapper.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly KeyRepositoryInterface $keyRepository,
    private readonly ?StateInterface $state = NULL,
  ) {
    $this->config = $this->configFactory->get('engaging_networks.rest_api');
    $this->client = new Client(
      $this->config->get('endpoint'),
      $this->keyRepository->getKey($this->config->get('api_key'))->getKeyValue(),
      cache: $this->config->get('cache_enable') ? $this->state : NULL,
      cache_key_token: $this->config->get('cache_keys.token'),
      cache_key_token_expire: $this->config->get('cache_keys.expire')
    );
  }

  /**
   * Gets the Engaging Networks REST API client.
   */
  public function getClient(): Client {
    return $this->client;
  }

}
