<?php

namespace Drupal\Tests\engaging_networks\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests the REST API settings form.
 *
 * @group engaging_networks
 */
class EngagingNetworksRestApiSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['engaging_networks', 'key'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * User with the permissions to edit module settings.
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer engaging networks']);
  }

  /**
   * Tests access to the REST API settings form.
   */
  public function testAccessSettingsForm(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(Url::fromRoute('engaging_networks.settings.rest_api'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
